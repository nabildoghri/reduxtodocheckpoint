import React from "react";
import AddTodo from "./components/AddTodo";
import TodoList from "./components/TodoList";
import VisibilityFilters from "./components/VisibilityFilters";
import "./styles.css";

export default function TodoApp() {
  return (
    <div className="todo-app">
      <div className="container m-5 p-2 rounded mx-auto bg-light shadow">
        <div className="row m-1 p-4">
          <div className="col">
            <div className="p-1 h1 text-primary text-center mx-auto display-inline-block">
              <h1>✔️ To Do List</h1>
              
            </div>
          </div>
        </div>
    
        <div className="row m-1 p-3">
          <div className="col col-11 mx-auto">
            <AddTodo />   
          </div>
        </div>
        <div className="row m-1 p-3 px-5 justify-content-end">
            <div className="col-auto d-flex align-items-center">
              <label className="text-secondary my-2 pr-2 view-opt-label">Filter</label>
              <VisibilityFilters /> 
            </div>
        </div>
        <div className="row mx-1 px-5 pb-3 w-80">
          <div className="col mx-auto">
            <TodoList />
          </div>
        </div>
      </div>
      
    </div>
  );
}
