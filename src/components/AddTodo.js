import React from "react";
import { connect } from "react-redux";
import { addTodo } from "../redux/actions";

class AddTodo extends React.Component {
  constructor(props) {
    super(props);
    this.state = { input: "" };
  }

  updateInput = input => {
    this.setState({ input });
  };

  handleAddTodo = () => {
    this.props.addTodo(this.state.input);
    this.setState({ input: "" });
  };

  render() {
    return (
      <div className="row bg-white rounded shadow-sm p-2 add-todo-wrapper align-items-center justify-content-center">
                <div className="col">
            <input className="form-control form-control-lg border-0 add-todo-input bg-transparent rounded" type="text"
              onChange={e => this.updateInput(e.target.value)}
              value={this.state.input}
            />
            </div>
        <div className="col-auto px-0 mx-0 mr-2">
        <button className="add-todo btn btn-primary" onClick={this.handleAddTodo}>
          Add Todo
        </button>
        </div>
        
      
      </div>
    );
  }
}

export default connect(
  null,
  { addTodo }
)(AddTodo);
// export default AddTodo;
